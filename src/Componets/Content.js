import '../CSS/Content.css';
import React from 'react';

function Content(props) {
    return (
        <div className="content">
            {props.children}
        </div>
    );
}

export default Content;