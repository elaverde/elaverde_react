import avatar from '../Images/avatar.png';
import doc from '../Documents/hv_edilson_laverde_molina.pdf';
import Icon from '@mdi/react';
import Scroll from '../Models/Scroll';

import { mdiClipboardAccount,mdiCodeArray,mdiBriefcase,mdiViewGridPlus,mdiDownload     } from '@mdi/js';
import '../CSS/Main.css';
function Main() {
    Scroll(
        (SPosT) => { 
            const root = document.querySelector(":root");
            root.style.setProperty("--progres-scroll-y",  SPosT + '%');
        }
    );
    
    return (
        <div  className="Main">
            <div className="Main_Progress_bar">

            </div>  
            <div className="Main_Info_basic">
                <img src={avatar} alt="Main_Avatar" className="Main_Avatar"/>
                <div className="Main_Name">Edilson Laverde Molina</div>
                <div className="Main_Rol">Desarrollador Web</div>
            </div>
            <ul className="Main_Dates">
                <li>
                    <a target="_blank"   href={doc}>
                        <Icon path={mdiDownload } className="Main_Icons"  size={1}/> 
                        Descargar       
                    </a>
                </li>        
                <li>
                    <a href="mailto:edilsonlaverde_182@hotmail.com">
                        <Icon path={mdiClipboardAccount} className="Main_Icons"  size={1}/>
                        Contacto 
                    </a>
                </li>        
            </ul>
            <ul className="Main_List">
                <li>
                    <a href="#about_me">
                        <Icon path={mdiClipboardAccount} className="Main_Icons"  size={1}/>
                        <p>Acerca de mi</p>      
                    </a>
                </li>
                <li>
                    <a href="#skills">
                        <Icon path={mdiCodeArray} className="Main_Icons"  size={1}/>
                        Skills
                    </a>
                </li>
                <li>
                    <a href="#experience">
                        <Icon path={mdiBriefcase } className="Main_Icons"  size={1}/>
                        Experiencia
                    </a>
                </li>
                <li>
                    <a href="#potafolio">
                        <Icon path={mdiViewGridPlus   } className="Main_Icons"  size={1}/>
                        Poratafolio
                    </a>
                </li>
            </ul>
        </div>
    );
}
export default Main;