import '../CSS/Portafolio.css';
import '../CSS/Modal.css';
import React, { useState } from 'react';
import Icon from '@mdi/react';
import { mdiXml } from '@mdi/js';
import { Demos } from '../JSON/demos';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import parse from 'html-react-parser';

const Portafolio = () => {
    
    const [open, setOpen] = useState(false);
    const [datamodal, setDataModal] = useState({
        title: "",
        html: "",
        banner: "",
        sub_title: "",
        link:""
    });
    const onOpenModal = (item) => {
        setDataModal(item);
        setOpen(true);
    };
    const onCloseModal = () => setOpen(false);
    const Path_Logos_Trabajo = require.context('../Images/trabajos', true);

    Demos.map((item) => { 
        item.src = Path_Logos_Trabajo(`./${item.logo}`);
        item.banner = Path_Logos_Trabajo(`./${item.poster}`);
        return item;
    });

    return (
        <div id="potafolio" className="content_box">
            <h1 className="content_box_title">
                <Icon path={mdiXml} className="Main_Icons" size={1} />
                Portafolio
            </h1>
            <div className="grid_portafolio">
                <Modal  open={open} onClose={onCloseModal} center>
                    <h1 className="title_modal">{datamodal.name}</h1>
                    <h2 className="sub_title_modal">{datamodal.sub_title}</h2>
                    <div className="content_modal">
                        <img className="poster_modal" src={datamodal.banner} alt=""/>
                        <p className="text_modal">
                            {parse(datamodal.html)}
                            <a target="_blank" href={datamodal.link} className="btn_modal">Ver</a>
                        </p>
                        
                    </div>
                </Modal>
                {Demos.map((item,index) => (
                <div onClick={ ()=> onOpenModal(item)} key={index} className="item_portafolio">
                    <img key={item.src} className="item_image_portafolio" src={item.src} alt={item.name} />
                    <div key={item.name} className="item_cortina">
                        <div key={index}  className="item_cortina_text">
                            {(item.name)}
                        </div>
                    </div>
                </div>
                ))}
            </div>
        </div>
    );
}

export default Portafolio;