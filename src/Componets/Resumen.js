import React from 'react';
import Icon from '@mdi/react';
import { mdiClipboardAccount,mdiBookEducation,mdiBriefcaseEdit } from '@mdi/js';
import {Trabajos} from '../JSON/trabajos';
import {Educacion} from '../JSON/educacion';
import '../CSS/Resumen.css';
class Resumen extends React.Component {

    
    render() {
        
        const Path_Logos_Educacion = require.context('../Images/educacion', true);
        const Path_Logos_Trabajo = require.context('../Images/trabajo', true);
        Educacion.map((item, index) => { 
            item.src = Path_Logos_Educacion(`./${item.logo}`);
        });
        Trabajos.map((item, index) => { 
            item.src = Path_Logos_Trabajo(`./${item.logo}`);
        });
        return (
            <div id="experience" className="content_box">
                <h1 className="content_box_title">
                    <Icon path={mdiClipboardAccount} className="Main_Icons" size={1} />
                    Resumen
                </h1>
                <div className="content_box_resumen">
                    <div className="column">
                        <h1 className="content_box_title">
                            <Icon path={mdiBriefcaseEdit} className="Main_Icons" size={1} />
                            Experiencia
                        </h1>
                        <ul className="content_box_resumen_list">
                            {
                                Trabajos.map((item, index) => (
                                    <li className="content_box_resumen_list_item" key={index}>
                                        <ul className={item.class}>
                                            <li>{item.fecha}</li>
                                            <li>{item.cargo}</li>
                                            <li>{item.empresa}</li>
                                            <li>{item.task}</li>
                                        </ul>
                                        <div className="content_box_resumen_icon_div">
                                            <img className="content_box_resumen_icon" src={item.src} />
                                        </div>
                                    </li>
                                   
                                ))
                            }
                        </ul>
                        
                    </div>
                    <div className="column">
                        <h1 className="content_box_title">
                            <Icon path={mdiBookEducation} className="Main_Icons" size={1} />
                            Estudios
                        </h1>
                        <ul className="content_box_resumen_list">
                            {
                                Educacion.map((item, index) => (
                                    <li className="content_box_resumen_list_item" key={index}>
                                        <ul className={item.class}>
                                            <li>{item.year}</li>
                                            <li>{item.institution}</li>
                                            <li>{item.title}</li>
                                            <li>{item.location}</li>
                                        </ul>
                                        <div className="content_box_resumen_icon_div">
                                            <img className="content_box_resumen_icon" src={item.src} />
                                        </div>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                </div> 
            </div>
        );
    }
}
export default Resumen;