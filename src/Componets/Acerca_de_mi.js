
import '../CSS/Background.css';
import React from 'react';
import Icon  from '@mdi/react';
import { mdiClipboardAccount,mdiCalendarRange,mdiBriefcaseOutline,mdiMapMarker,mdiMapMarkerRadius,mdiCodeBraces,mdiPencilRuler,mdiFaceAgent,mdiAccountGroup} from '@mdi/js';



// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";



class Acerca_de_mi extends React.Component {

    render() {
        const Path_Carrusel = require.context('../Images', true);
        const Images_Carrusel = [
            './client_1.png',
            './client_2.png',
            './client_3.png',
            './client_4.png'
        ];
        var Images_src = [];
        Images_Carrusel.map((item, index) => {
            Images_src.push(Path_Carrusel(`${item}`));
        });

        

        
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1
        };
        var currentTime = new Date();
        var year = currentTime.getFullYear();
        var year = year - 1986;
        return (
            <div id="about_me" className="content_box">
                <h1 className="content_box_title">
                    <Icon path={mdiClipboardAccount} className="Main_Icons" size={1} />
                    Acerca de mi
                </h1>
                <p className="content_box_text">
                    Ingeniero Sistemas, con Amplia experiencia en lenguajes PHP, JavaScript, python dominio de HyperText Markup Language versión 5 y anteriores CSS3 manejo de técnicas como Ajax librerías  como JQUERY, Greensock, Framework Angular, VUEJS, manejo de material e-learning “SCORM” conocimientos en administración de bases de datos en MYSQL, Postgresql, con una alta capacidad de adaptarse a nuevos lenguajes.
                </p>
                <p className="content_box_text">
                    Excelentes relaciones personales, liderazgo, adaptabilidad al cambio, orientado a la obtención de resultados y calidad de trabajo, aprendizaje continuo y trabajo en equipo.
                </p>
                <ul className="content_box_list">
                    <li>

                        <Icon path={mdiCalendarRange} className="Main_Icons" size={1} />
                        <span>
                            <b>Años:</b> {year}
                        </span>
                       
                    </li>
                    <li>
                        <Icon path={mdiBriefcaseOutline} className="Main_Icons" size={1} />
                        <span><b>Freenlacer</b></span>
                    </li>
                    <li>
                        <Icon path={mdiMapMarker} className="Main_Icons" size={1} />
                        
                        <span><b>Residente:</b> Colombia</span>
                    </li>
                    <li>
                        <Icon path={mdiMapMarkerRadius} className="Main_Icons" size={1} />
                        <p><b>Dirección:</b> Fusagasugá</p>
                    </li>
                </ul>
                <h1 className="content_box_title">
                    <Icon path={mdiFaceAgent} className="Main_Icons" size={1} />
                    <span><b>Mis servicios</b></span>
                </h1>
                <ul className="content_box_list">
                    <li className="content_box_sub_title">
                        <Icon path={mdiPencilRuler} className="Main_Icons" size={1} />
                        <span><b>DISEÑO DE PÁGINAS WEB</b></span>
                    </li>
                    <li className="content_box_text">Un diseño actual, limpio y estructurado te ayudará a conseguir nuevos clientes en internet.</li>
                    <li className="content_box_sub_title">
                        <Icon path={mdiCodeBraces} className="Main_Icons" size={1} />
                        <span><b>PROGRAMACIÓN WEB A MEDIDA</b></span>
                    </li>
                    <li className="content_box_text">Desarrollo a medida de cualquier herramienta online que necesites tener en tu página web.</li>
                </ul>
                <h1 className="content_box_title">
                    <Icon path={mdiAccountGroup} className="Main_Icons" size={1} />
                    <span><b>Clientes</b></span>
                </h1>
                <div className="content_box_carrusel">
                    <Slider {...settings}>
                        {
                            Images_src.map((src,index) => (
                                <div key="div-{index}" >
                                    <img className="content_box_carrusel_img" key="img-{index}" src={src} />
                                </div>
                            ))
                        }
                    </Slider>
                </div>    
                
            </div>
        );
    }
    
}

export default Acerca_de_mi;