import '../CSS/Skill.css';
import Icon from '@mdi/react';
import { ReactOriginalIcon, GitPlainIcon,VuejsPlainIcon, PhpPlainIcon, JavascriptPlainIcon,MoodlePlainIcon, PythonPlainIcon, NodejsPlainIcon,Html5PlainIcon,Css3PlainIcon } from "react-devicons";
import { mdiXml } from '@mdi/js';
import ProgressBar from "@ramonak/react-progress-bar";
function Skill() {
    return (
        <div id="skills" className="content_box">
            <h1 className="content_box_title">
                <Icon path={mdiXml} className="Main_Icons" size={1} />
                Skills
            </h1>
            <div className="Skills">
                <ul className="Skill_List">
                    <li>
                        <label className="Skill_Label">
                            <GitPlainIcon color="#000" />
                            Git
                        </label>
                        <ProgressBar
                            completed={80}
                            bgColor="#20CE65"
                            height="14px"
                            borderRadius="10px"
                            labelAlignment="center"
                            baseBgColor="#19232B"
                            labelColor="#040913"
                            labelSize="10"
                            margin="2"
                            padding="2"
                            transitionDuration="3s"
                            transitionTimingFunction="ease"
                            animateOnRender
                            dir="auto"
                            maxCompleted={100}
                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <ReactOriginalIcon color="#000" />
                            React</label>
                        <ProgressBar
                             completed={20}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="2s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}
                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <VuejsPlainIcon color="#000" />
                            Vuejs</label>
                        <ProgressBar
                             completed={60}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="3s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}
                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <PhpPlainIcon color="#000" />
                            PHP</label>
                        <ProgressBar
                             completed={67}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="2s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}
                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <JavascriptPlainIcon color="#000" />
                            JavaScript</label>
                        <ProgressBar
                             completed={90}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="2s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}
                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <MoodlePlainIcon color="#000" />
                            Moodle</label>
                        <ProgressBar
                             completed={75}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="3s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}
                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <PythonPlainIcon color="#000" />
                            Python</label>
                        <ProgressBar
                             completed={55}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="2s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <NodejsPlainIcon color="#000" />
                            Nodejs</label>
                        <ProgressBar
                             completed={59}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="2s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}
                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <Html5PlainIcon color="#000" />
                            HTML5</label>
                        <ProgressBar
                             completed={69}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="3s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}
                        />
                    </li>
                    <li>
                        <label className="Skill_Label">
                            <Css3PlainIcon color="#000" />
                            CSS3</label>
                        <ProgressBar
                             completed={79}
                             bgColor="#20CE65"
                             height="14px"
                             borderRadius="10px"
                             labelAlignment="center"
                             baseBgColor="#19232B"
                             labelColor="#040913"
                             labelSize="10"
                             margin="2"
                             padding="2"
                             transitionDuration="3s"
                             transitionTimingFunction="ease"
                             animateOnRender
                             dir="auto"
                             maxCompleted={100}
                        />
                    </li>
                </ul>
            </div>
        </div>
    );
}
export default Skill;