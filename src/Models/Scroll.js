export default function Scroll(callback) {
    window.addEventListener('scroll', function (element) {
        let SPosT = ((document.documentElement.scrollTop + document.body.scrollTop) / (document.documentElement.scrollHeight - document.documentElement.clientHeight) * 100);
        callback(SPosT);
    });
}