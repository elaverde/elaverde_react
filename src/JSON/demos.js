export const Demos = [
    {
        "name":"Qplay",
        "poster":"work1.png",
        "logo":"logo_work1.svg",
        "title":"Avisoft Aplicación Movil",
        "sub_title":"Herramienta e-learning",
        "html":"Herramienta que permite configurar  videos de youtube para que los alumnos reafirmen los conocimientos expuestos en los videos de una forma fácil, la evaluación se realiza por medio de objetos de aprendizaje SCORM.",
        "link":"https://qplay.icu/",
        "tipo":"app-web"
    },
    {
        "name":"Avisoft",
        "poster":"work4.jpg",
        "logo":"logo_work4.png",
        "title":"Avisoft Aplicación Movil",
        "sub_title":"Aplicación Avicola",
        "html":"En esta app podrás ver las noticias de fenavi, calcular el consumo de acuerdo a la edad de las aves, ver tablas de peso según la edad y la raza, calcular su producción de huevos ideal estas son algunas de la razas que manejamos en nuestra app. Babcock Brown, Hy-line, ISA Browm, Pollo de engorde italcol hembras y machos",
        "link":"https://play.google.com/store/apps/details?id=elaverde.avisoft2&hl=es",
        "tipo":"app-movil"
    }
   
    ,
    {
        "name":"Calculadora Salud y Pensión",
        "poster":"work3.jpg",
        "logo":"logo_work3.png",
        "title":"Calculadora Salud y Pensión Aplicación Movil",
        "sub_title":"Aplicación Financiera",
        "html":"¿Cómo sé cuanto debo pagar?  Si usted es trabajador independiente que devenga un salario y quiere saber los porcentajes que debe pagar por salud, pensión y riesgos laborales esta aplicación le ayudara con esa tarea.",
        "link":"http://bit.ly/2Udj16h",
        "tipo":"app-movil"
    }
    ,
    {
        "name":"SAGYM",
        "poster":"work2.png",
        "logo":"logo_work2.png",
        "title":"SAGYM Aplicación WEB",
        "sub_title":"Aplicación Avicola",
        "html":"<b> SAGYM, </b> es un sistema administrativo diseñado especialmente para gimnasios, es una herramienta que se ajusta a las necesidades del gimnasio, aumentar las ventas y ofrecer un mejor servicio al usuario o cliente.",
        "link":"http://bit.ly/2FIqe6t",
        "tipo":"app-web"
    },
    {
        "name":"UDECO",
        "poster":"work6.jpg",
        "logo":"logo_work6.png",
        "title":"UDECO Aplicación WEB",
        "sub_title":"Aplicación educativa",
        "html":"UDECO aplicación de videos para estudiantes de la universidad de Cundinamarca, este servicio permite generar test y evaluar cada segundo de aprendizaje, además de ello permite generar SCORM compatibles con los distintos  LMS del mercado. <iframe width='100%' height='260' src='https://www.youtube.com/embed/JEbiN3A8540' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>",
        "link":"http://bit.ly/2uGNY4B",
        "tipo":"app-web"
    },
   {
        "name":"MATFIN",
        "poster":"work7.png",
        "logo":"logo_work7.png",
        "title":"MATFIN Aplicación WEB",
        "sub_title":"Aplicación Financiera",
        "html":"Herramienta informática para la enseñanza de la matemática financiera, dirigida por Doctor Carlos Fernando Cometa.",
        "link":"http://matfincarlosf.com/",
        "tipo":"app-web"
    },
    {
        "name":"Game Conway",
        "poster":"work17.jpg",
        "logo":"logo_work16.jpg",
        "title":"Game Conway",
        "sub_title":"Pasatiempos",
        "html":"El prolífico matemático inglés John Conway inventó en 1970 el juego de Vida. El jugador únicamente tiene que colocar algunas fichas en un tablero cuadriculado. Esta configuración inicial de fichas evolucionará a lo largo del tiempo siguiendo las “leyes de Conway“. Conway eligió sus reglas de manera que fuera muy difícil predecir qué ocurriría a partir de una configuración inicial, y son bastante sencillas. Teniendo en cuenta que cada casilla tiene 8 casillas adyacentes. En cuanto a esta última posibilidad, en el artículo se explica que Conway creía que no existía una configuración inicial finita que creciera ilimitadamente, y ofrecía un premio de 50 dólares a quien fuera capaz de probar o refutar su conjetura. Según Gardner, la respuesta de los hackers a su artículo fue de tal calibre que estimaba que la manía de explorar las formas de Vida había costado millones de dólares en uso ilícito de ordenadores (recordemos que entonces los ordenadores eran máquinas muy caras, que sólo se podían permitir las empresas o las universidades). Fue Bill Gosper, uno de los hackers del MIT que trabajaba en el laboratorio de Inteligencia Artificial quien consiguió demostrar que Conway estaba equivocado al idear un cañón de deslizadores, una configuración de fichas que da lugar a deslizadores cada cierto tiempo, por lo que el número de fichas en el tablero cada vez es mayor.",
        "link":"pasa_tiempos/conway/index.html",
        "tipo":"app-web"
    },
    {
        "name":"​Anatomía Topográfica​ del Sistema Óseo Bovino",
        "poster":"work5.png",
        "logo":"logo_work5.png",
        "title":"Ova Anatomía Topográfica​ del Sistema Óseo Bovino",
        "sub_title":"Zootecnia",
        "html":"Esta OVA busca ​fortalecer en los estudiantes los conocimientos relacionados con la anatomía topográfica del sistema óseo bovino, identificando las partes y regiones anatómicas en las que éste se divide.",
        "link":"http://bit.ly/2uyma2e",
        "tipo":"ova"
    },
    {
        "name":"​CADI",
        "poster":"work8.jpg",
        "logo":"logo_work8.png",
        "title":"OVA CADI",
        "sub_title":"Aprendizaje",
        "html":"Objeto virtual que que muestra como construir un cadi en el campo de aprendizaje diciplinar.",
        "link":"http://bit.ly/2HZZujo",
        "tipo":"ova"
    },
    {
        "name":"Beginners English Course",
        "poster":"work9.jpg",
        "logo":"logo_work9.png",
        "title":"OVA Beginners English Course",
        "sub_title":"Idiomas",
        "html":"OVA para diplomado Beginners English Course, el cual busca afianzar conocimientos por medio de una conversación de oficina.",
        "link":"http://bit.ly/2FypjEa",
        "tipo":"ova"
    },
     {
        "name":"Simple Present",
        "poster":"work10.jpg",
        "logo":"logo_work10.png",
        "title":"OVA Simple Present",
        "sub_title":"Idiomas",
        "html":"OVA presente simple, el cual busca afianzar conocimientos por medio de un juego de cajas.",
        "link":"http://bit.ly/2JQuXqK",
        "tipo":"ova"
    },
    {
        "name":"THIS THAT THESE THOSE",
        "poster":"work11.jpg",
        "logo":"logo_work11.jpg",
        "title":"OVA THIS THAT THESE THOSE",
        "sub_title":"Idiomas",
        "html":"OVA this, that, these y those,  el cual busca afianzar conocimientos por medio de un juego asociación de situaciones",
        "link":"http://bit.ly/2JM5m1Y",
        "tipo":"ova"
    },
    {
        "name":"Beginners English Course",
        "poster":"work12.jpg",
        "logo":"logo_work12.png",
        "title":"Beginners English Course",
        "sub_title":"Idiomas",
        "html":"OVA para diplomado Beginners English Course, el cual busca afianzar conocimientos por medio de unas situaciones típicas de un parque.",
        "link":"http://bit.ly/2FJyxyz",
        "tipo":"ova"
    },
    {
        "name":"Supervisores e inventores",
        "poster":"work13.png",
        "logo":"logo_work13.jpg",
        "title":"Red Supervisores e inventores",
        "sub_title":"Institucional",
        "html":"OVA que busca lograr que el usuario conozca el concepto de interventoría y comprenda las funciones que desempeñan un supervisor y un interventor dentro de un proceso contractual.",
        "link":"http://bit.ly/2TNjUOR",
        "tipo":"ova"
    },
    {
        "name":"Manual de Moodle",
        "poster":"work14.png",
        "logo":"logo_work14.jpg",
        "title":"Red Manual de Moodle",
        "sub_title":"Manuales",
        "html":"OVA que busca mostrar las distintas herramientas que ofrece Moodle ya sea para estudiantes o para profesores",
        "link":"http://bit.ly/2CM2oom",
        "tipo":"ova"
    },
    {
        "name":"Oferta Académica Universidad de Cundinamarca",
        "poster":"work15.png",
        "logo":"logo_work15.png",
        "title":"Red Oferta Académica Universidad de Cundinamarca",
        "sub_title":"Institucional",
        "html":"Presentación que busca mostrar la oferra académica de la Universidad Cundinamarca",
        "link":"http://bit.ly/2OzsWOk",
        "tipo":"ova"
    }
]