export const Trabajos = [
    
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "21 de Julio de 2021 - 17 de Diciembre de 2021",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    },
    
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "10 de Febrero de 2021 - 18 de Junio de 2021",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    },
    {
        "empresa": "SENA",
        "cargo": "Gestor Repositorio",
        "fecha": "08 de Febrero de 2021 – 27 de Diciembre 2021",
        "task": "Gestor LMS",
        "class": "",
        "logo": "sena.png"
    },
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "03 de Agosto de 2020 - 18 de Diciembre de 2020",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    },
    
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "05 de Marzo de 2020 - 19 de Junio de 2020",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    },
    {
        "empresa": "SENA",
        "cargo": "Gestor Repositorio",
        "fecha": "28 de Febrero de 2020 – 27 de Diciembre 2020",
        "task": "Gestor LMS",
        "class": "",
        "logo": "sena.png"
    },
    {
        "empresa": "SENA",
        "cargo": "Gestor Repositorio",
        "fecha": "02 de Octuble de 2019 – 31 de Diciembre 2019",
        "task": "Gestor LMS",
        "class": "",
        "logo": "sena.png"
    },
    
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "13 de Febrero de 2019 - 21 de Junio de 2019",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "active",
        "logo": "udec.png"
    }, {
        "empresa": "Corporación Universitaria Iberoamericana",
        "cargo": "Desarrollador templates digitales",
        "fecha": "9 de Octubre de 2018 - 9 de Diciembre de 2018",
        "task": "Developer WEB",
        "class": "",
        "logo": "udec.png"
    }, {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": " Desarrollador recursos digitales",
        "fecha": "15 de Marzo de 2018 - 19 de Julio de 2018",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    },
    {
        "empresa": "SENA",
        "cargo": "Gestor Repositorio",
        "fecha": "20 de Febrero de 2018 – 31 de Diciembre 2018",
        "task": "Gestor LMS",
        "class": "",
        "logo": "sena.png"
    },
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "15 de Febrero de 2017 - 19 de Julio de 2017",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    }, {
        "empresa": "Start Fitness (Fusagasugá)",
        "cargo": "Desarrollador web",
        "fecha": "15 de Febrero de 2017",
        "task": "Developer WEB - Administrador BD",
        "class": "active",
        "logo": "start.png"
    },
    {
        "empresa": "SENA",
        "cargo": "Gestor LMS “Learning Management System”",
        "fecha": "20 de Febrero de 2017 – 15 de Diciembre 2017",
        "task": "Gestor LMS",
        "class": "",
        "logo": "sena.png"
    },
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "20 de Febrero de 2016 - 19 de Diciembre de 2016",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    },
    {
        "empresa": "SENA",
        "cargo": "Supervisor LMS“ Learning Management System",
        "fecha": "20 de Febrero de 2016 – 30 de Diciembre 2016",
        "task": "Supervisor LMS",
        "class": "",
        "logo": "sena.png"
    },
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "25 de Febrero de 2015 - 19 de Diciembre de 2015",
        "task": "eveloper WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"

    },
    {
        "empresa": "SENA",
        "cargo": "Desarrollador de Línea de Producción(Desarrollador recursos digitales)",
        "fecha": "20 de Febrero de 2015 – 30 de Diciembre 2015",
        "task": "Developer WEB",
        "class": "",
        "logo": "sena.png"
    },
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales.",
        "fecha": "25 de Febrero de 2014 - 19 de Diciembre de 2014",
        "task": "Developer WEB - Administrador LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    },
    {
        "empresa": "SENA",
        "cargo": "Desarrollador de Línea de Producción(Desarrollador recursos digitales)",
        "fecha": "20 de Enero de 2014 – 20 de Diciembre 2014",
        "task": "Developer WEB",
        "class": "",
        "logo": "sena.png"
    },
    {
        "empresa": "Universidad de Cundinamarca (Fusagasugá)",
        "cargo": "Desarrollador recursos digitales",
        "fecha": "2 de Agosto de 2013 – 20 de diciembre 2013",
        "task": "Developer WEB - Administrator LMS MOODLE",
        "class": "",
        "logo": "udec.png"
    },
    {
        "empresa": "SENA",
        "cargo": "Instructor",
        "fecha": "05 de mayo de 2013 – 20 de diciembre 2013",
        "task": "Docente",
        "class": "",
        "logo": "sena.png"
    },


    {
        "empresa": "FUNDACION MANASES",
        "cargo": "Docente área sistemas",
        "fecha": "05 Febrero 2009 – 30 Febrero 2013",
        "task": "Docente área sistemas",
        "class": "",
        "logo": "manases.png"
    },
    {
        "empresa": "INVERSIONES LAVERDE MOLINA HERMANOS Y CIA",
        "cargo": "Gerente Comercial",
        "fecha": "01 septiembre 2006 - En la Actualidad",
        "task": "Manejo de inventarios Comercialización de productos avícolas",
        "class": "active",
        "logo": "avisoft.png"
    }
]