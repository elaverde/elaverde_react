export const Educacion = [
    {
        "year": "2022",
        "institution": "Platzi",
        "location": "Online",
        "title": "Marca personal",
        "logo":"platzi.png"

    },
    {
        "year": "2022",
        "institution": "Platzi",
        "location": "Online",
        "title": "Introducción al financiamiento de startups",
        "logo":"platzi.png"

    },
    {
        "year": "2022",
        "institution": "Platzi",
        "location": "Online",
        "title": "Curso de fundamentos de ingeniería de software",
        "logo":"platzi.png"

    },
    {
        "year": "2022",
        "institution": "Platzi",
        "location": "Online",
        "title": "Curso de marketing digital",
        "logo":"platzi.png"

    },
    {
        "year": "2022",
        "institution": "Platzi",
        "location": "Online",
        "title": "Curso de marketing voz a voz",
        "logo":"platzi.png"

    },
    {
        "year": "2019",
        "institution": "Platzi",
        "location": "Online",
        "title": "Curso de programación básica",
        "logo":"platzi.png"

    },
    {
        "year": "2019",
        "institution": "Udemy",
        "location": "Online",
        "title": "CERTIFICACIÓN: VPS seguro con DigitalOcean, Nginx, Letsencrypt Ubuntu",
        "logo":"udemy.png"

    },
    {
        "year": "2016",
        "institution": "COGNOS ONLINE",
        "location": "Bogotá (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: Transferencia de conocimientos para desarrolladores building block Cognos Online",
        "logo":"cognos.jpg"
    },
    {
        "year": "2015",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: NIVEL BASICO - ORIENTAR PROCESOS FORMATIVOS PRESENCIALES CON BASE EN LOS PLANES DE FORMACION CONCERTADOS.",
        "logo":"sena.png"
    },

    {
        "year": "2015",
        "institution": "MinTIC vive digital",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: DESARROLLO NATIVO EN ANDROID",
        "logo":"mintic.png"
    },
    {
        "year": "2015",
        "institution": "MinTIC vive digital",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: PROGRAMMING IN HTML5 WIHT JAVACERIPT AND CSS3",
        "logo":"mintic.png"
    },
    {
        "year": "2014",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: NIVEL AVANZADO - ANALIZAR LOS REQUISITOS DEL CLIENTE PARA CONSTRUIR EL SISTEMA DE INFORMACION.",
        "logo":"sena.png"
    },
    {
        "year": "2014",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: FUNDAMENTOS DE LA VIGILANCIA TECNOLOGICA Y SU IMPORTANCIA EN EL PROCESO DE LA INVESTIGACION APLICADA",
        "logo":"sena.png"
    },
    {
        "year": "2013",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: APLICACION DE HERRAMIENTAS METODOLOGICAS EN INVESTIGACION: PROCESOS DE CIENCIA, TECNOLOGIA E INNOVACION",
        "logo":"sena.png"
    },
    {
        "year": "2013",
        "institution": "Universidad de Cundinamarca",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: TALLER II JORNADA DE SENSIBILIZACIÓN EN TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN (TIC) UNIVERSIDAD DE CUNDINAMARCA",
        "logo":"udec.png"
    },
    {
        "year": "2013",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: MODELADO Y RENDERIZACION DE ESPACIOS ARQUITECTONICOS Y ESCENARIOS PARA VIDEOJUEGOS.",
        "logo":"sena.png"
    },
    {
        "year": "2013",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: FORMACION TECNOPEDAGOGICA EN AMBIENTES VIRTUALES DE APRENDIZAJE BLACKBOARD 9.1",
        "logo":"sena.png"
    },
    {
        "year": "2013",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: INDUCCIÓN A PROCESOS PEDAGÓGICOS",
        "logo":"sena.png"
    },
    {
        "year": "2012",
        "institution": "UNIVERSIDAD DE CUNDINAMARCA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "Ingeniero de Sistemas",
        "logo":"udec.png"
    },
    {
        "year": "2009",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: LINUX: SISTEMA OPERATIVO, COMANDOS Y UTILIDAD.",
        "logo":"sena.png"
    },
    {
        "year": "2009",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: DESARROLLO DE APLICACIONES CON INTERFAZ GRÁFICA, MANEJO",
        "logo":"sena.png"
    },
    {
        "year": "2009",
        "institution": "SENA",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "CERTIFICACIÓN: DE EVENTOS, CLASES Y OBJETOS: JAVA",
        "logo":"sena.png"
    },
    {
        "year": "2005",
        "institution": "INSTITUTO TÉCNICO INDUSTRIAL",
        "location": "FUSAGASUGÁ (CUNDINAMARCA)",
        "title": "Bachiller Técnico",
        "logo":"iti.png"
    }
]